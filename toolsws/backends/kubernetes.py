import json
import os
import subprocess
import sys
import time
from datetime import datetime
from enum import Enum
from functools import lru_cache
from typing import Any, ClassVar, Dict, Optional

import requests
import yaml
from toolforge_weld.kubernetes import K8sClient, parse_quantity
from toolforge_weld.kubernetes_config import Kubeconfig

from toolsws.backends.backend import Backend
from toolsws.tool import Tool
from toolsws.utils import wait_for
from toolsws.wstypes import (
    GenericWebService,
    JSWebService,
    LighttpdWebService,
    PythonWebService,
)

DEFAULT_HTTP_PORT = 8000
STARTED_AT_ANNOTATION = "toolforge.org/started-at"


class MountOption(Enum):
    ALL = "all"
    NONE = "none"

    def __str__(self) -> str:
        return self.value

    @property
    def supports_non_buildservice(self) -> bool:
        """Determines whether this option can be used on non-buildservice images."""
        return self == MountOption.ALL

    def get_labels(self) -> Dict[str, str]:
        return {"toolforge.org/mount-storage": self.value}


class KubernetesRoutingHandler:
    """Create and manage service and ingress objects to route HTTP requests."""

    DEFAULT_PUBLIC_DOMAIN: ClassVar[str] = "toolforge.org"

    def __init__(
        self,
        *,
        api: K8sClient,
        tool: Tool,
        namespace: str,
        webservice_config: Dict[str, Any],
        extra_labels: Optional[Dict[str, str]] = None,
    ):
        self.api = api
        self.tool = tool
        self.namespace = namespace

        self.domain = webservice_config.get(
            "public_domain", self.DEFAULT_PUBLIC_DOMAIN
        )

        # Labels for all objects created by this webservice
        # TODO: unduplicate
        self.webservice_labels = {
            "app.kubernetes.io/component": "web",
            "app.kubernetes.io/managed-by": "webservice",
            "toolforge": "tool",
            "name": self.tool.name,
        }

        self.webservice_label_selector = {
            k: v
            for k, v in self.webservice_labels.items()
            if k not in ["toolforge", "name"]
        }

        # this is after label_selector is created, to make sure we don't have conflicting
        # objects from different sources
        if extra_labels is not None:
            self.webservice_labels.update(extra_labels)

    def _find_objs(self, kind, selector):
        """
        Return objects of kind matching selector, or None if they don't exist.

        Objects that are currently being deleted by the Kubernetes service
        (meaning they have a non-empty metadata.deletionTimestamp value) are
        ignored.
        """
        objs = self.api.get_objects(kind, label_selector=selector)
        # Ignore objects that are in the process of being deleted.
        return [
            o
            for o in objs
            if o["metadata"].get("deletionTimestamp", None) is None
        ]

    def _get_ingress_subdomain(self):
        """
        Returns the full spec of the domain-based routing ingress object for
        this webservice
        """
        return {
            "apiVersion": "networking.k8s.io/v1",
            "kind": "Ingress",
            "metadata": {
                "name": "{}-subdomain".format(self.tool.name),
                "namespace": self.namespace,
                "labels": self.webservice_labels,
            },
            "spec": {
                "rules": [
                    {
                        "host": f"{self.tool.name}.{self.domain}",
                        "http": {
                            "paths": [
                                {
                                    "path": "/",
                                    "pathType": "Prefix",
                                    "backend": {
                                        "service": {
                                            "name": self.tool.name,
                                            "port": {
                                                "number": DEFAULT_HTTP_PORT,
                                            },
                                        },
                                    },
                                },
                            ],
                        },
                    },
                ],
            },
        }

    def _get_svc(self, target_port=DEFAULT_HTTP_PORT, selector=None):
        """
        Return full spec for the webservice service
        """
        service = {
            "kind": "Service",
            "apiVersion": "v1",
            "metadata": {
                "name": self.tool.name,
                "namespace": self.namespace,
                "labels": self.webservice_labels,
            },
            "spec": {
                "ports": [
                    {
                        "name": "http",
                        "protocol": "TCP",
                        "port": DEFAULT_HTTP_PORT,
                        "targetPort": target_port,
                    }
                ],
            },
        }

        if selector:
            service["spec"]["selector"] = selector

        return service

    def _start_common(
        self, target_port=DEFAULT_HTTP_PORT, service_selector=None
    ):
        """Create objects used for routing to a web service on any backend."""
        svcs = self._find_objs("services", self.webservice_label_selector)
        if len(svcs) == 0:
            self.api.create_object(
                "services", self._get_svc(target_port, service_selector)
            )

        ingresses = self._find_objs(
            "ingresses", self.webservice_label_selector
        )
        if len(ingresses) == 0:
            self.api.create_object("ingresses", self._get_ingress_subdomain())

    def start_kubernetes(self):
        """Create objects required to route traffic to a Kubernetes webservice."""
        self._start_common(service_selector={"name": self.tool.name})

    def stop(self):
        """Clean up any created objects."""
        self.api.delete_objects(
            "ingresses", label_selector=self.webservice_label_selector
        )
        self.api.delete_objects(
            "services", label_selector=self.webservice_label_selector
        )


class KubernetesBackend(Backend):
    """
    Backend spawning webservices with a k8s Deployment + Service
    """

    DEFAULT_RESOURCES = {
        "default": {
            "limits": {
                "memory": "512Mi",
                "cpu": "0.5",
            },
            "requests": {
                # Pods are guaranteed at least this many resources
                "memory": "256Mi",
                "cpu": "0.125",
            },
        },
        "jdk": {
            "limits": {
                # Higher Memory Limit for JDK based webservices, but not
                # higher request, so it can use more memory before being
                # killed, but will die when there is a memory crunch.
                "memory": "1Gi",
                "cpu": "0.5",
            },
            "requests": {
                # Pods are guaranteed at least this many resources
                "memory": "256Mi",
                "cpu": "0.125",
            },
        },
    }

    CONFIG_VARIANT_KEY = "webservice"
    # TODO: make configurable
    CONFIG_IMAGE_TAG = "latest"
    CONFIG_SUPPORTED_WS_TYPES = {
        "generic": GenericWebService,
        "js": JSWebService,
        "lighttpd": LighttpdWebService,
        # TODO: remove -plain variant, difference now handled in images
        "lighttpd-plain": LighttpdWebService,
        "python": PythonWebService,
    }

    DEFAULT_BUILD_SERVICE_REGISTRY = "tools-harbor.wmcloud.org"

    @staticmethod
    @lru_cache()
    def get_types():
        client = K8sClient(
            kubeconfig=Kubeconfig.load(), user_agent="webservice"
        )
        configmap = client.get_object(
            "configmaps", "image-config", namespace="tf-public"
        )
        yaml_data = yaml.safe_load(configmap["data"]["images-v1.yaml"])

        types = {
            "buildservice": {
                "cls": GenericWebService,
                "resources": KubernetesBackend.DEFAULT_RESOURCES["default"],
                "use_webservice_runner": False,
                "state": "stable",
            }
        }

        for name, data in yaml_data.items():
            if KubernetesBackend.CONFIG_VARIANT_KEY not in data["variants"]:
                continue

            variant = data["variants"][KubernetesBackend.CONFIG_VARIANT_KEY]
            resources = variant["extra"].get("resources", "default")

            # TODO: this dict might benefit from being a separate class
            # or an instance of WebService directly
            types[name] = {
                "cls": KubernetesBackend.CONFIG_SUPPORTED_WS_TYPES[
                    variant["extra"]["wstype"]
                ],
                "image": "{image}:{tag}".format(
                    image=variant["image"],
                    tag=KubernetesBackend.CONFIG_IMAGE_TAG,
                ),
                "resources": KubernetesBackend.DEFAULT_RESOURCES[resources],
                "state": data["state"],
            }

        return types

    def __init__(
        self,
        tool,
        wstype,
        webservice_config,
        buildservice_image=None,
        mem=None,
        cpu=None,
        replicas=1,
        mount=MountOption.ALL,
        health_check_path=None,
        extra_args=None,
    ):
        super(KubernetesBackend, self).__init__(
            tool, wstype, extra_args=extra_args
        )

        config = KubernetesBackend.get_types()[self.wstype]

        self.project = Tool.get_current_project()
        self.webservice = config["cls"](tool, extra_args)

        if self.wstype == "buildservice":
            self.container_image = "{registry}/{image}".format(
                registry=webservice_config.get(
                    "buildservice_repository",
                    self.DEFAULT_BUILD_SERVICE_REGISTRY,
                ),
                image=buildservice_image,
            )
        else:
            self.container_image = config["image"]

        self.container_resources = config["resources"]
        self.use_webservice_runner = config.get("use_webservice_runner", True)

        if mem:
            dec_mem = parse_quantity(mem)
            if dec_mem < parse_quantity("256Mi"):
                self.container_resources["requests"]["memory"] = mem
            else:
                self.container_resources["requests"]["memory"] = str(
                    dec_mem / 2
                )
            self.container_resources["limits"]["memory"] = mem

        if cpu:
            dec_cpu = parse_quantity(cpu)
            if dec_cpu < parse_quantity("250m"):
                self.container_resources["requests"]["cpu"] = cpu
            else:
                self.container_resources["requests"]["cpu"] = str(dec_cpu / 2)
            self.container_resources["limits"]["cpu"] = cpu

        self.replicas = replicas
        self.api = K8sClient(
            kubeconfig=Kubeconfig.load(), user_agent="webservice"
        )
        self.routing_handler = KubernetesRoutingHandler(
            api=self.api,
            tool=self.tool,
            namespace=self._get_ns(),
            webservice_config=webservice_config,
        )

        self.mount = mount

        self.health_check_path = health_check_path

        # Labels for all objects created by this webservice
        self.webservice_labels = {
            "app.kubernetes.io/component": "web",
            "app.kubernetes.io/managed-by": "webservice",
            "toolforge": "tool",
            "name": self.tool.name,
        }

        self.webservice_label_selector = {
            k: v
            for k, v in self.webservice_labels.items()
            if k not in ["toolforge", "name"]
        }

        self.webservice_labels.update(self.mount.get_labels())

    def _get_ns(self):
        return "tool-{}".format(self.tool.name)

    def _find_objs(self, kind, selector):
        """
        Return objects of kind matching selector, or None if they don't exist.

        Objects that are currently being deleted by the Kubernetes service
        (meaning they have a non-empty metadata.deletionTimestamp value) are
        ignored.
        """
        objs = self.api.get_objects(kind, label_selector=selector)
        # Ignore objects that are in the process of being deleted.
        return [
            o
            for o in objs
            if o["metadata"].get("deletionTimestamp", None) is None
        ]

    # see https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#podsecuritycontext-v1-core
    def _get_pod_security_context(self) -> dict[str, Any]:
        tool_uid = self.tool.uid

        return {
            "fsGroup": tool_uid,
            "runAsGroup": tool_uid,
            "runAsNonRoot": True,
            "runAsUser": tool_uid,
            "seccompProfile": {"type": "RuntimeDefault"},
        }

    # see https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#securitycontext-v1-core
    def _get_container_security_context(self) -> dict[str, Any]:
        tool_uid = self.tool.uid

        return {
            "allowPrivilegeEscalation": False,
            "capabilities": {
                "drop": ["ALL"],
            },
            "privileged": False,
            # RW container root fs so tmp files can be created without additional volume mounts
            "readOnlyRootFilesystem": False,
            "runAsGroup": tool_uid,
            "runAsNonRoot": True,
            "runAsUser": tool_uid,
        }

    def _get_deployment(self, started_at):
        """
        Return the full spec of the deployment object for this webservice,
        started at the given timestamp.
        """
        if self.use_webservice_runner:
            cmd = [
                "/usr/bin/webservice-runner",
                "--type",
                self.webservice.name,
                "--port",
                str(DEFAULT_HTTP_PORT),
            ]
        else:
            cmd = []

        if self.extra_args:
            cmd.extend(self.extra_args)

        ports = [
            {
                "name": "http",
                "containerPort": DEFAULT_HTTP_PORT,
                "protocol": "TCP",
            }
        ]

        if self.health_check_path:
            probes = {
                "startupProbe": {
                    "httpGet": {
                        "port": "http",
                        "path": self.health_check_path,
                        "httpHeaders": [
                            {
                                "name": "User-Agent",
                                "value": "toolforge-startup-check/1.0.0",
                            },
                        ],
                    },
                    "initialDelaySeconds": 0,
                    "periodSeconds": 1,
                    "failureThreshold": 120,
                },
                "livenessProbe": {
                    "httpGet": {
                        "port": "http",
                        "path": self.health_check_path,
                        "httpHeaders": [
                            {
                                "name": "User-Agent",
                                "value": "toolforge-liveness-check/1.0.0",
                            },
                        ],
                    },
                    "initialDelaySeconds": 0,
                    "periodSeconds": 10,
                    "failureThreshold": 3,
                },
            }
        else:
            probes = {
                "startupProbe": {
                    "tcpSocket": {"port": "http"},
                    "initialDelaySeconds": 0,
                    "periodSeconds": 1,
                    "failureThreshold": 120,
                },
                "livenessProbe": {
                    "tcpSocket": {"port": "http"},
                    "initialDelaySeconds": 0,
                    "periodSeconds": 10,
                    "failureThreshold": 3,
                },
            }

        return {
            "kind": "Deployment",
            "apiVersion": "apps/v1",
            "metadata": {
                "name": self.tool.name,
                "namespace": self._get_ns(),
                "labels": self.webservice_labels,
            },
            "spec": {
                "replicas": self.replicas,
                "selector": {"matchLabels": self.webservice_label_selector},
                "template": {
                    "metadata": {
                        "labels": self.webservice_labels,
                        "annotations": {
                            # this annotation, which changes each time the deployment is updated,
                            # will cause Kubernetes to cleanly restart the deployment
                            # (start new pod, wait for it to be ready, stop old pod)
                            STARTED_AT_ANNOTATION: started_at,
                        },
                    },
                    "spec": {
                        "securityContext": self._get_pod_security_context(),
                        **self._get_container_spec(
                            "webservice",
                            self.container_image,
                            cmd,
                            self.container_resources,
                            ports,
                            probes=probes,
                        ),
                    },
                },
            },
        }

    def _get_shell_pod(self, name):
        """Get the specification for an interactive pod."""
        cmd = ["/bin/bash", "-il"]
        if self.extra_args:
            cmd = self.extra_args

        labels = {
            "name": name,
            "app.kubernetes.io/component": "webservice-interactive",
            "app.kubernetes.io/managed-by": "webservice",
            "app.kubernetes.io/version": "2",
            "toolforge": "tool",
        }
        labels.update(self.mount.get_labels())

        return {
            "apiVersion": "v1",
            "kind": "Pod",
            "metadata": {
                "namespace": self._get_ns(),
                "name": name,
                "labels": labels,
            },
            "spec": self._get_container_spec(
                name,
                self.container_image,
                cmd,
                resources=self.container_resources,
                ports=None,
                stdin=True,
                tty=True,
            ),
        }

    def _get_container_spec(
        self,
        name,
        container_image,
        cmd,
        resources,
        ports=None,
        stdin=False,
        tty=False,
        probes=None,
    ):
        """Get the specification for a container."""
        spec = {
            "containers": [
                {
                    "name": name,
                    "image": container_image,
                    "ports": ports,
                    "resources": resources,
                    "tty": tty,
                    "stdin": stdin,
                    "securityContext": self._get_container_security_context(),
                }
            ]
        }
        if cmd:
            spec["containers"][0]["command"] = cmd

        if probes:
            spec["containers"][0].update(probes)

        if self.wstype == "buildservice":
            spec["containers"][0]["env"] = [
                # For regular images, webservice-runner would take care of configuring the HTTP
                # server to listen on port 8000, but for buildservice ones we need to set the
                # PORT env var as it's what most buildpacks will use by default.
                {
                    "name": "PORT",
                    "value": str(DEFAULT_HTTP_PORT),
                },
                {
                    "name": "NO_HOME",
                    "value": "a buildservice pod does not need a home env",
                },
            ]
        else:
            spec["containers"][0]["workingDir"] = (
                "/data/project/{toolname}/".format(toolname=self.tool.name)
            )

        return spec

    def _any_pod_in_state(self, podlist, state):
        """
        Returns true if any pod in the input list of pods are in a given state
        """
        for pod in podlist:
            if pod["status"]["phase"] == state:
                return True

        return False

    def request_start(self):
        self.webservice.check(self.wstype)

        deployments = self._find_objs(
            "deployments", self.webservice_label_selector
        )
        if len(deployments) == 0:
            started_at = datetime.utcnow().isoformat()
            self.api.create_object(
                "deployments", self._get_deployment(started_at)
            )

        self.routing_handler.start_kubernetes()

    def _delete_deployment(self):
        self.api.delete_objects(
            "deployments", label_selector=self.webservice_label_selector
        )
        self.api.delete_objects(
            "replicasets",
            label_selector={"name": self.tool.name},
        )
        self.api.delete_objects(
            "pods", label_selector=self.webservice_label_selector
        )

    def request_stop(self):
        self.routing_handler.stop()

        self._delete_deployment()

    def request_restart(self):
        print("Restarting", end="")

        pods_killed = False

        # To restart the Kubernetes application, we replace the Deployment object.
        # At least the startedAt timestamp is always different,
        # so Kubernetes will always create a new pod and stop the old one,
        # even if the rest of the Deployment did not change.
        # (But it is also possible that the replace command also specified
        # updated container resources, such as CPU or memory, which changed.)

        started_at = datetime.utcnow().isoformat()

        try:
            self.api.replace_object(
                "deployments", self._get_deployment(started_at)
            )
        except requests.exceptions.HTTPError as e:
            # An HTTP 422 (Unprocessable Entity) indicates that we can't
            # replace the current deployment for some reason. likely it's an
            # old deployment and something has changed since then. If that
            # happens we just delete the pods to get the immediate restart
            # done and ask the user to file a bug.
            if e.response.status_code != 422:
                raise e

            print(
                "\nWARNING: Unable to gracefully restart this tool, using a "
                "more forced approach. If this persists please file a bug report: "
                # link is to https://wikitech.wikimedia.org/wiki/Help:Cloud_Services_communication
                "https://w.wiki/6Zuu"
            )
            print("Please include the following in the bug report:")
            print(f" {e.response.text}")

            self._delete_deployment()
            self.api.create_object(
                "deployments", self._get_deployment(started_at)
            )

            pods_killed = True

        def check_running():
            nonlocal pods_killed

            pods = [
                pod
                for pod in self._find_objs(
                    "pods", self.webservice_label_selector
                )
                if (
                    pod["metadata"]["annotations"].get(STARTED_AT_ANNOTATION)
                    == started_at
                )
            ]

            # If the restart is done, that's great.
            if self._any_pod_in_state(pods, "Running"):
                return True

            # If not, let's see if we can figure out what's the problem.

            deployment_data = self.api.get_object(
                "deployments", self.tool.name
            )
            if not deployment_data:
                # I can't think of why this would be missing, but let's check for it either way.
                # It'll time out anyway, and maybe we'll get a bug report about that particular
                # scenario.
                return False

            deployment_conditions = deployment_data["status"].get(
                "conditions", []
            )
            for condition in deployment_conditions:
                if (
                    not pods_killed
                    and condition["type"] == "ReplicaFailure"
                    and condition["reason"] == "FailedCreate"
                    and condition["status"] == "True"
                    and "forbidden: exceeded quota" in condition["message"]
                ):
                    # Looks like the namespace is out of quota. (T341100)
                    print(
                        "\nWARNING: Using a more forced method to restart the pods"
                        " since this tool is almost out of quota. This might result"
                        " in more downtime during the restart.",
                        file=sys.stderr,
                    )

                    self.api.delete_objects(
                        "pods", label_selector=self.webservice_label_selector
                    )
                    pods_killed = True

                # TODO: handle the case where there was another thing trying to create pods and
                # that another thing uses the freed up quota before after stopping the old pods.

            # TODO: Check the pod data if the job is just straight up crashing. jobs-api has code for
            # that, which can be adapter for this repo (or better yet) ported to toolforge-weld.

            return False

        return wait_for(check_running, None, 30)

    def get_state(self):
        # TODO: add some state concept around ingresses
        pods = self._find_objs("pods", self.webservice_label_selector)
        if self._any_pod_in_state(pods, "Running"):
            return Backend.STATE_RUNNING
        if self._any_pod_in_state(pods, "Pending"):
            return Backend.STATE_PENDING
        svcs = self._find_objs("services", self.webservice_label_selector)
        deployment = self._get_live_deployment()
        if len(svcs) != 0 and deployment:
            return Backend.STATE_PENDING
        else:
            return Backend.STATE_STOPPED

    def _get_live_deployment(self):
        deployments = self._find_objs(
            "deployments", self.webservice_label_selector
        )
        if len(deployments) == 1:
            return deployments[0]
        return None

    def shell(self) -> int:
        """Run an interactive container on the k8s cluster."""
        name = "shell-{}".format(int(time.time()))

        shell_env = os.environ.copy()
        shell_env["GOMAXPROCS"] = "1"

        # --overrides is used because there does not seem to be any other way
        # to tell `kubectl run` what workingDir to use for the container. This
        # is annoying, but mostly reasonable as a typical Docker image would
        # set a consistent workingDir in its definition rather than the
        # backwards compatible with grid engine system that Toolforge uses of
        # mounting in an external $HOME.
        cmd = [
            "kubectl",
            "run",
            name,
            "--attach=true",
            "--stdin=true",
            "--tty=true",
            "--restart=Never",
            "--rm=true",
            "--wait=true",
            "--quiet=true",
            "--pod-running-timeout=1m",
            "--image={}".format(self.container_image),
            "--overrides={}".format(json.dumps(self._get_shell_pod(name))),
            "--command=true",
            "--",
        ]
        if self.extra_args:
            cmd.extend(self.extra_args)
        else:
            cmd.extend(["/bin/bash", "-il"])

        try:
            kubectl = subprocess.Popen(cmd, env=shell_env)
            kubectl.wait()
            return kubectl.returncode
        finally:
            # Belt & suspenders cleanup just in case kubectl leaks the pod
            self.api.delete_objects("pods", label_selector={"name": name})
