import os

from .ws import WebService


class PythonWebService(WebService):
    NAME = "uwsgi-python"

    def check(self, wstype):
        src_path = self.tool.get_homedir_subpath("www/python/src")
        if not os.path.exists(src_path):
            raise WebService.InvalidWebServiceException(
                "Could not find ~/www/python/src. Are you sure you have a "
                "proper uwsgi application in ~/www/python/src?"
            )
