from .ws import WebService


class GenericWebService(WebService):
    NAME = "generic"

    def __init__(self, tool, extra_args=None):
        super(GenericWebService, self).__init__(tool, extra_args)

    def check(self, wstype):
        return self.extra_args is not None
