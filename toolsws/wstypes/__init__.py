from .generic import GenericWebService
from .js import JSWebService
from .lighttpd import LighttpdWebService
from .python import PythonWebService
from .ws import WebService

WSTYPES = {
    cls.NAME: cls  # type: ignore
    for cls in [
        GenericWebService,
        JSWebService,
        LighttpdWebService,
        PythonWebService,
    ]
}


__all__ = list(WSTYPES.values()) + [WebService]  # type: ignore
