import os

from .ws import WebService


class LighttpdWebService(WebService):
    """A Lighttpd web server"""

    NAME = "lighttpd-plain"

    def check(self, wstype):
        # Check for a .lighttpd.conf file or a public_html
        public_html_path = self.tool.get_homedir_subpath("public_html")
        lighttpd_conf_path = self.tool.get_homedir_subpath(".lighttpd.conf")
        if not (
            os.path.exists(public_html_path)
            or os.path.exists(lighttpd_conf_path)
        ):
            raise WebService.InvalidWebServiceException(
                "Could not find a public_html folder or a .lighttpd.conf "
                "file in your tool home."
            )
