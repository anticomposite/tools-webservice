import argparse
import functools
import os
import re
import sys
import textwrap

import yaml
from toolforge_weld.logs import get_log_source
from toolforge_weld.utils import peek

from toolsws.backends import Backend, KubernetesBackend
from toolsws.backends.kubernetes import MountOption
from toolsws.config import load_config
from toolsws.tool import Tool
from toolsws.utils import wait_for
from toolsws.wstypes import WebService

BACKEND_DEFAULT = "kubernetes"
MOUNT_DEFAULT = MountOption.ALL
KUBERNETES_DEFAULT_WSTYPE = "php7.4"

# Make all `print()` output go to stderr by default
print = functools.partial(print, file=sys.stderr)


def generate_default_buildservice_image_name(tool):
    buildservice_image = tool.manifest.get("buildservice-image", None)
    if buildservice_image is None:
        buildservice_image = "{0}/{0}:latest".format(f"tool-{tool.name}")
    return buildservice_image


def format_wstypes_block(config, default, leader="\n  * "):
    """Pretty print a list of runtime wstypes for a backend."""
    wstypes = []
    for name, spec in config.items():
        if spec.get("state", "active") == "deprecated":
            continue

        extra = ""
        if name == default:
            extra = " (default)"
        wstypes.append("{}{}".format(name, extra))
    return leader.join(sorted(wstypes))


def kube_quant(string):
    """
    A type for args that roughly matches up with Kubernetes' quantity.go
    General form is <number><suffix>
    The following are acceptable suffixes

    base1024: Ki | Mi | Gi | Ti | Pi | Ei
    base1000: n | u | m | "" | k | M | G | T | P | E
    """
    valid_suffixes = [
        "Ki",
        "Mi",
        "Gi",
        "Ti",
        "Pi",
        "Ei",
        "n",
        "u",
        "m",
        "",
        "k",
        "M",
        "G",
        "T",
        "P",
        "E",
    ]
    pattern = re.compile(r"^(\d+)([A-Za-z]{0,2})$")
    quant_check = pattern.match(string)
    if quant_check:
        suffix = quant_check.group(2)
        if suffix in valid_suffixes:
            return string

    msg = "{} is not a valid Kubernetes quantity".format(string)
    raise argparse.ArgumentTypeError(msg)


def start(job, message):
    try:
        job.request_start()
    except WebService.InvalidWebServiceException as e:
        raise SystemExit(str(e)) from e
    # FIXME: Treat pending state differently.
    return wait_for(lambda: job.get_state() == Backend.STATE_RUNNING, message)


def stop(job, message):
    job.request_stop()
    return wait_for(lambda: job.get_state() == Backend.STATE_STOPPED, message)


def update_manifest(job: Backend, action: str, args: argparse.Namespace):
    """
    Update a tool's service manifest to indicate the type of webservice in use

    :param action 'start' or 'stop'
    :param args parsed cli arguments
    """
    if action == "start":
        mount = job.mount if hasattr(job, "mount") else MountOption.ALL
        health_check_path = (
            job.health_check_path
            if hasattr(job, "health_check_path")
            else None
        )

        # TODO: can we replace this with some logic that just saves
        # the file when there's a change?
        if (
            "web" not in job.tool.manifest
            or job.tool.manifest["web"] != job.wstype
            or (
                job.wstype == "buildservice"
                and (
                    job.tool.manifest.get("buildservice-image", None)
                    != args.buildservice_image
                    or job.tool.manifest.get("mount", None) != mount.value
                )
            )
            or (
                job.wstype != "buildservice"
                and (
                    "buildservice-image" in job.tool.manifest
                    or "mount" in job.tool.manifest
                )
            )
            or health_check_path != job.tool.manifest.get("health-check-path")
        ):
            job.tool.manifest["web"] = job.wstype
            if job.wstype == "buildservice":
                job.tool.manifest["buildservice-image"] = (
                    args.buildservice_image
                )
                job.tool.manifest["mount"] = mount.value
            else:
                if "buildservice-image" in job.tool.manifest:
                    del job.tool.manifest["buildservice-image"]
                if "mount" in job.tool.manifest:
                    del job.tool.manifest["mount"]
            if health_check_path:
                job.tool.manifest["health-check-path"] = health_check_path
            elif "health-check-path" in job.tool.manifest:
                del job.tool.manifest["health-check-path"]
            if args.replicas != 1:
                job.tool.manifest["replicas"] = args.replicas
            if hasattr(job, "container_resources"):
                # Not using the value directly to avoid persisting values from
                # the old Kubernetes cluster
                if args.cpu is not None:
                    job.tool.manifest["cpu"] = args.cpu
                if args.memory is not None:
                    job.tool.manifest["memory"] = args.memory
            if job.extra_args:
                job.tool.manifest["web::extra_args"] = job.extra_args

            job.tool.save_manifest()
    elif action == "stop":
        if "web" in job.tool.manifest:
            for key in [
                "distribution",  # no longer set, drop from old files
                "release",
                "web",
                "buildservice-image",
                "mount",
                "backend",
                "cpu",
                "memory",
                "replicas",
                "health-check-path",
                "web::extra_args",
            ]:
                if key in job.tool.manifest:
                    del job.tool.manifest[key]
            job.tool.save_manifest()
    else:
        # blow up!
        raise Exception("action has to be 'start' or 'stop', got %s" % action)


def find_service_template():
    """Look for service.template in different directories"""
    dirs = ["~", "~/www/python/src", "~/www/js", "~/public_html"]
    found = []
    for dir_ in dirs:
        path = os.path.expanduser(os.path.join(dir_, "service.template"))
        if os.path.exists(path):
            found.append(path)

    if len(found) > 1:
        # Multiple templates found, error
        print(
            "Only one service.template file is allowed, multiple were found. "
            "You will need to remove or rename the others."
        )
        for path in found:
            print("* %s" % path)
        sys.exit(1)
    elif len(found) == 1:
        return found[0]
    else:
        # No template
        return None


def main():
    description = """
    Online documentation: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web

    Supported webservice types:
      * {types}
    """.format(
        types=format_wstypes_block(
            KubernetesBackend.get_types(),
            KUBERNETES_DEFAULT_WSTYPE,
            leader="\n      * ",
        ),
    )

    tool = Tool.from_currentuser()

    # The bootstrap parser is used to look for a --template=... arg. If one is
    # found, or if it exists in one of the default directories, then it is
    # loaded as a YAML file and used to inject defaults into the main argument
    # parser.
    bootstrap = argparse.ArgumentParser(add_help=False)
    bootstrap.add_argument(
        "--template",
        dest="template_file",
        type=argparse.FileType(mode="r"),
        help="service.template to use when starting a webservice",
    )

    toolforge_cli_in_use = "TOOLFORGE_CLI" in os.environ

    argparser = argparse.ArgumentParser(
        description=textwrap.dedent(description),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        parents=[bootstrap],
        prog="toolforge webservice" if toolforge_cli_in_use else None,
    )

    # This option is kept for backwards compat (+ possible future use for jobs-api migration).
    argparser.add_argument(
        "--backend",
        choices=["kubernetes"],
        help=argparse.SUPPRESS,
    )
    argparser.add_argument(
        "wstype",
        nargs="?",
        metavar="TYPE",
        help="Type of webservice to start",
    )

    allowed_actions = ["start", "stop", "status", "restart", "shell", "logs"]
    argparser.add_argument(
        "action",
        choices=allowed_actions,
        metavar="ACTION",
        help="Action to perform: {}".format(", ".join(allowed_actions)),
    )
    argparser.add_argument(
        "extra_args",
        nargs="*",
        metavar="...",
        help="Extra arguments to be parsed by the chosen TYPE",
    )
    # Backwards compat with older webservice.
    # Allow --canonical cli argument, but don't do anything with it.
    argparser.add_argument(
        "--canonical", action="store_true", help=argparse.SUPPRESS
    )
    argparser.add_argument(
        "-m",
        "--mem",
        required=False,
        type=kube_quant,
        help="Set higher memory limit",
        dest="memory",
    )
    argparser.add_argument(
        "-c",
        "--cpu",
        required=False,
        type=kube_quant,
        help="Set a higher cpu limit",
        dest="cpu",
    )
    argparser.add_argument(
        "-r",
        "--replicas",
        required=False,
        type=int,
        default=1,
        help="Set the number of pod replicas to use",
    )
    argparser.add_argument(
        "--buildservice-image",
        default=generate_default_buildservice_image_name(tool),
        help="Set image to use for buildservice type. Defaults to '%(default)s'",
    )
    argparser.add_argument(
        "--mount",
        required=False,
        type=MountOption,
        choices=list(MountOption),
        help=f"Set which shared storage (NFS) directories to mount to this pod. Defaults to '{MOUNT_DEFAULT.value}'",
    )
    argparser.add_argument(
        "--health-check-path",
        required=False,
        type=str,
        default=None,
        help=(
            "Relative URL for the HTTP health check of the webservice. If the webservice fails to respond several "
            "times to this URL, it will be restarted. Note that your webservice will have to return 200 OK to any "
            "'Host' header on that url. Example: `--health-check-path /healthz`. If not passed, a simple TCP "
            "check will be used instead."
        ),
    )

    logs_group = argparser.add_argument_group("Log viewer options")
    logs_group.add_argument(
        "-f",
        "--follow",
        required=False,
        action="store_true",
        help="Stream updates",
    )
    logs_group.add_argument(
        "-l",
        "--last",
        required=False,
        type=int,
        help="Number of recent log lines to display",
    )

    template_extra_args = None
    # Use the bootstrap argparser to check for a --template=... arg
    bootstrap_args, remainder_argv = bootstrap.parse_known_args()
    if not bootstrap_args.template_file:
        template = find_service_template()
        if template:
            bootstrap_args.template_file = open(template, mode="r")
    if bootstrap_args.template_file:
        # Load settings from the template file and validate them by running
        # them through argparser as though they had been passed in the command
        # line.
        tmpl = yaml.safe_load(bootstrap_args.template_file)
        tmpl_argv = []
        if "backend" in tmpl:
            tmpl_argv.append("--backend={}".format(tmpl["backend"]))
        if "cpu" in tmpl:
            tmpl_argv.append("--cpu={}".format(tmpl["cpu"]))
        if "mem" in tmpl:
            tmpl_argv.append("--mem={}".format(tmpl["mem"]))
        if "replicas" in tmpl:
            tmpl_argv.append("--replicas={}".format(tmpl["replicas"]))
        if "buildservice-image" in tmpl:
            tmpl_argv.append(
                "--buildservice-image={}".format(tmpl["buildservice-image"])
            )
        if "mount" in tmpl:
            tmpl_argv.append("--mount={}".format(tmpl["mount"]))
        if "health-check-path" in tmpl:
            tmpl_argv.append(
                "--health-check-path={}".format(tmpl["health-check-path"])
            )
        if "type" in tmpl:
            tmpl_argv.append(tmpl["type"])

        tmpl_argv.append("start")  # always parse template as a 'start' action

        if "extra_args" in tmpl:
            tmpl_argv.append("--")
            tmpl_argv.extend(tmpl["extra_args"])

        class ThrowingArgumentParser(argparse.ArgumentParser):
            """ArgumentParser subclass that throws errors rather than exiting.

            From https://stackoverflow.com/a/14728477/8171
            """

            def error(self, message):
                raise ValueError(message)

        parser = ThrowingArgumentParser(add_help=False, parents=[argparser])
        try:
            template_args = parser.parse_args(tmpl_argv)
            # If parse_args() passed, set the values as defaults for the real
            # argument parsing phase.
            argparser.set_defaults(
                backend=template_args.backend,
                cpu=template_args.cpu,
                memory=template_args.memory,
                replicas=template_args.replicas,
                wstype=template_args.wstype,
                buildservice_image=template_args.buildservice_image,
                mount=template_args.mount,
                health_check_path=template_args.health_check_path,
            )
            # The argument gathering behavior of extra args does not work as
            # a default on the parser. It also will cause problems if the
            # template is providing TYPE as ArgumentParser will get confused
            # about where the extra args start. This seems to be the case even
            # if using a "--" sentinal token in the argument list to indicate
            # that everything after that should be treated as a positional
            # argument. This is really a flaw in the CLI of this tool
            # (optional positional arguments), but fixing that now is
            # a potentially large communications problem.
            template_extra_args = template_args.extra_args
        except ValueError as e:
            print(
                "Your template file ({}) contains errors:".format(
                    bootstrap_args.template_file.name
                )
            )
            print("  {}".format(str(e)))
            sys.exit(1)

    webservice_config = load_config()

    args = argparser.parse_args(args=remainder_argv)

    # Select the first of:
    # * --backend=... from cli args
    # * 'backend' from service.template
    # * 'backend' from service.manifest
    backend = args.backend
    if backend is None:
        backend = tool.manifest.get("backend", BACKEND_DEFAULT)

    if args.action != "shell":
        if "backend" in tool.manifest and tool.manifest["backend"] != backend:
            manifest_file = tool.get_homedir_subpath("service.manifest")
            print(
                "Backend '{}' from {} does not match '{}'".format(
                    tool.manifest["backend"],
                    manifest_file,
                    backend,
                )
            )
            print("  Try stopping your current webservice:")
            print("    webservice stop")
            print("  Then try starting it again:")
            print("    {}".format(" ".join(sys.argv)))
            print(
                "  If you have already tried that and it did not help, "
                "remove the state file before retrying:"
            )
            print("    rm {}".format(manifest_file))
            sys.exit(1)

    if args.wstype is None:
        args.wstype = tool.manifest.get("web", KUBERNETES_DEFAULT_WSTYPE)

    if args.wstype == "buildservice" and args.buildservice_image is False:
        print(
            "--buildservice-image must be provided when webservice type is 'buildservice'."
        )
        print("Review the arguments passed and try again")
        sys.exit(1)

    if not args.mount:
        if args.wstype != "buildservice":
            args.mount = MountOption.ALL
        elif "mount" in tool.manifest:
            args.mount = MountOption(tool.manifest.get("mount"))
        else:
            print(
                "ERROR: --mount not explicitly specified on a build service based tool"
            )
            print(
                "  To disable mounting NFS shared storage (recommended if possible) use '--mount none'."
            )
            print("  To continue using shared storage use '--mount all'.")
            print(
                "  For help refer to <https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service>"
            )
            sys.exit(1)
    if (
        args.wstype != "buildservice"
        and not args.mount.supports_non_buildservice
    ):
        print("This --mount option is only supported for 'buildservice' type")
        print("Review the arguments passed and try again")
        sys.exit(1)

    if args.cpu is None:
        args.cpu = tool.manifest.get("cpu", None)

    if args.memory is None:
        args.memory = tool.manifest.get("memory", None)

    if args.replicas == 1:
        args.replicas = tool.manifest.get("replicas", 1)

    if args.health_check_path is None:
        args.health_check_path = tool.manifest.get("health-check-path", None)

    acceptable_wstypes = KubernetesBackend.get_types()

    if not args.extra_args:
        args.extra_args = tool.manifest.get(
            "web::extra_args", template_extra_args
        )

    if args.wstype not in acceptable_wstypes.keys():
        print("type must be one of:")
        print(format_wstypes_block(acceptable_wstypes, ""))
        sys.exit(1)

    job = KubernetesBackend(
        tool,
        args.wstype,
        buildservice_image=args.buildservice_image,
        mount=args.mount,
        mem=args.memory,
        cpu=args.cpu,
        replicas=args.replicas,
        extra_args=args.extra_args,
        health_check_path=args.health_check_path,
        webservice_config=webservice_config,
    )

    tool.manifest["backend"] = backend

    if job.is_deprecated(args.wstype):
        print("DEPRECATED: '{}' type is deprecated.".format(args.wstype))
        print(
            "  See https://wikitech.wikimedia.org/wiki/Help:Toolforge/{}".format(
                "Kubernetes" if backend == "kubernetes" else "Web"
            )
        )
        print("  for currently supported types.")

    if args.action == "start":
        if job.get_state() != Backend.STATE_STOPPED:
            print("Your job is already running")
            sys.exit(1)

        start(job, "Starting webservice")
        update_manifest(job, "start", args)

    elif args.action == "stop":
        if job.get_state() == Backend.STATE_STOPPED:
            print("Your webservice is not running", file=sys.stdout)
        else:
            stop(job, "Stopping webservice")
        update_manifest(job, "stop", args)

    elif args.action == "restart":
        if job.get_state() != Backend.STATE_RUNNING:
            start(job, "Your job is not running, starting")
            update_manifest(job, "start", args)
        else:
            if not job.request_restart():
                print(
                    "Your webservice is taking quite while to restart. If it isn't"
                    " up shortly, run a 'webservice stop' and the start command"
                    " used to run this webservice to begin with.",
                )
                sys.exit(1)

            update_manifest(job, "start", args)
        tool.save_manifest()

    elif args.action == "status":
        if job.get_state() != Backend.STATE_STOPPED:
            print(
                "Your webservice of type {} is running on backend {}".format(
                    job.wstype, backend
                ),
                file=sys.stdout,
            )
        else:
            print("Your webservice is not running", file=sys.stdout)

    elif args.action == "shell":
        sys.exit(job.shell())

    elif args.action == "logs":
        log_source = get_log_source(user_agent="webservice")
        logs = log_source.query(
            selector=job.webservice_label_selector,  # type: ignore
            follow=args.follow,
            lines=args.last,
        )

        first, logs = peek(logs)
        if not first:
            print("No logs found!")

        try:
            for entry in logs:
                if entry.container != "webservice":
                    continue
                time = entry.datetime.replace(microsecond=0).isoformat("T")
                print(f"{time} [{entry.pod}] {entry.message}", file=sys.stdout)
        except KeyboardInterrupt:
            raise SystemExit("Interrupted")
