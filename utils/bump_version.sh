#!/usr/bin/env bash

set -o errexit
set -o pipefail

if [[ "${1}" == "--no-cache" ]]; then
    no_cache=(--no-cache)
    shift
fi

DOCKER="docker"
extra_options=(
        "--volume=$PWD:/src:rw"
)
if command -v podman >/dev/null; then
    DOCKER="podman"
    extra_options=(
        "--userns=keep-id"
        "--volume=$PWD:/src:rw,z"
    )
fi


email="$(git config user.email)"
name="$(git config user.name)"
if [[ "${1}" == "bookworm" ]]; then
    distro="${1}"
    shift
else
    distro="bookworm"
fi

$DOCKER build "utils/debuilder-${distro}" "${no_cache[@]}" -t "debuilder-${distro}:latest"
$DOCKER run \
    --entrypoint /generate_changelog.sh \
    --env "EMAIL=${email}" \
    --env "NAME=${name}" \
    --rm \
    "--user=$UID" \
    "${extra_options[@]}" \
    "debuilder-${distro}:latest" \
    "$@"

new_version="$(head -n1 debian/changelog | grep -o '[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+')"
new_message="$(sed '/^toolforge-/,$!d;/^ *--/q' debian/changelog | awk 'NR > 2 {print prev} {prev=$0}')"
latest_tag="$(
    git ls-remote --refs --tags origin \
    | awk '{print $2}' | sort -V | tail -n 1 | sed -e 's/refs\/tags\///'
)"
bugs="$(
    git log "$latest_tag"..HEAD | grep -o "Bug: T[[:digit:]]*" || :
)"

git commit --signoff --all --message "d/changelog: bump to $new_version

$new_message

$bugs
Git-Dch: Ignore"

git tag "debian/$new_version"
