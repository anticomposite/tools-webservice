# tools-webservices

This Debian package will deploy the `webservice` command line interface
which enables Toolforge users to host and interact with webservices in Toolforge.

It is tipically installed in Toolforge bastions servers.

Documentation about how to use the `webservice` utility can be found at:

https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web

## Building the debian packages

The process will be:

- Create new branch
- Bump the version
  - Update the `debian/changelog` and `setup.py` (done by `bump_version.sh`)
  - Create a patch, get it reviewed and merged
- Create a tag (`debian/<new_version>`) and push
- Build the package (done by `build_deb.sh`)
- Upload the package to the toolforge repositories

Let's get started!

### Create new branch

To get started, create a new branch from main:

```bash
~:$ git checkout -b <new-branch-name>
```

### Update the changelog and setup.py

To do so, you can run the script:

```bash
~:$ utils/bump_version.sh
```

That will:

- create an entry in `debian/changelog` from the git log since the last `debian/*` tag
- bump the version in `setup.py` too

At this point, you should create a commit and send it for review, and continue once merged..

```bash
~:$  git commit -m "Bumped version to <new_version>" --signoff
~:$  git push -u origin <new-branch-name>
```

### Get the version bump commit merged

Review the `changelog` and the `setup.py` changes to make sure it's what
you want (it uses your name, email, etc.), and ask for reviews.

### Push the debian tag

The `bump_version.sh` script will have created a tag named `debian/<new_version>` for you.
You need to push it to the repository (i.e `git push origin debian/<new_version>`).

### Build the package

#### With containers

This is the recommended way of building the package, as it's agnostic of the
OS/distro you are using.

It will not allow you to sign your package though, so if you need that try using
the manual process.

Now you can build the package with:

```bash
~:$ utils/build_deb.sh
```

The first time it might take a bit more time as it will build the core image to
build packages, downloading many dependencies. The next run it will not need to
download all those dependencies, so it will be way faster.

**NOTE**: If it failed when installing packages, try passing `--no-cache` to
force rebuilding the cached layers.

### Build the package

#### From CI

There's a job running on ci that will build the package for you too, you can download it from there.

#### With containers

This is the recommended way of building the package, as it's agnostic of the
OS/distro you are using.

It will not allow you to sign your package though, so if you need that try using
the manual process.

Now you can build the package with:

```bash
~:$ utils/build_deb.sh
```

The first time it might take a bit more time as it will build the core image to
build packages, downloading many dependencies. The next run it will not need to
download all those dependencies, so it will be way faster.

**NOTE**: If it failed when installing packages, try passing `--no-cache` to
force rebuilding the cached layers.

Additional documentation on the wmcs-package-build script is available at
<https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Packaging#wmcs-package-build>

### Uploading to the toolforge repository

Once you have built the package you want, you can uploade it following:
<https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Packaging#Uploading_a_package>


## Development environment

Some notes on the development of tools-webservice.

* After you've cloned this repo and before you make your first changes,
  follow the instructions here https://pre-commit.com/ to install and
  setup pre-commit on your local machine.

Most people developing this package ends up reusing Toolsbeta
as shared development environment. For that:
* read https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Toolsbeta
* pick a placeholder tool (like: test, test2, test3)
* let others know that you are testing something in Toolsbeta
* upload your changes to Gitlab
* pull your changes from Gitlab into the toolsbeta bastion and install it 
  (Note that you might need root access for this).
* become the placeholder tool and run your tests!
  (Note that you might need to check more than one test tool because 
  there is no guarantee that a user is not currently running a webservice 
  with the your test tool of choice)

It is difficult to have a working setup of tools-webservice
on a local laptop for development. You would need to replicate most of
the components described here:
https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Kubernetes/Networking_and_ingress

It would involve:

* having a local kubernetes cluster (minikube, kind, etc)
* having a working ingress-nginx setup like the one deployed to Toolforge
** from https://gitlab.wikimedia.org/repos/cloud/toolforge/ingress-nginx
* having a setup similar to the one used in the jobs-framework-api:
** https://gitlab.wikimedia.org/repos/cloud/toolforge/jobs-framework-api/-/blob/main/README.md

